
counter = 3
fib_prev = 1
fib_cur = 2

while len(str(fib_cur)) < 1000:
    counter += 1
    next = fib_prev + fib_cur
    fib_prev = fib_cur
    fib_cur = next

print(f"%d: %d" % (counter, fib_cur))
