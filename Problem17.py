numberWords = {
    '1': 'one',
    '2': 'two',
    '3': 'three',
    '4': 'four',
    '5': 'five',
    '6': 'six',
    '7': 'seven',
    '8': 'eight',
    '9': 'nine',
    '0': 'zero',
    '10': 'ten',
    '11': 'eleven',
    '12': 'twelve',
    '13': 'thirteen',
    '14': 'fourteen',
    '15': 'fifteen',
    '16': 'sixteen',
    '17': 'seventeen',
    '18': 'eighteen',
    '19': 'nineteen',
    '20': 'twenty',
    '30': 'thirty',
    '40': 'forty',
    '50': 'fifty',
    '60': 'sixty',
    '70': 'seventy',
    '80': 'eighty',
    '90': 'ninety',
}


# n an integer (<=9999)
# dictionary that maps numbers to words
def toNumerWords(n, w):
    if n > 9999:
        return None

    word = ""
    # make number 4 digits long
    zeroNStr = f"%04d" % n

    # now split in part
    # p3 = Tausender
    p3 = zeroNStr[0]
    # p3 = Hunderter
    p2 = zeroNStr[1]
    # p2 = Zehner und einer, also 2. stellig
    p1 = zeroNStr[2:]
    # p11 = Zehner
    p11 = zeroNStr[2] + '0'
    # p22 = Einer
    p12 = zeroNStr[3]

    # does the first digit exist
    if int(p3) > 0:
        word += f"%s thousand " % w[p3]

    # does the second digit exist
    if int(p2) > 0:
        word += f"%s hundred " % w[p2]

    # Zehner ?
    if (int(p1) > 0):
        word1 = ""
        # special treatment for all numbers below 20
        if int(p1) < 20:
            word1 += f"%s" % w[str(int(p1))]
        else:
            if int(p12) > 0:
                word1 += f"%s-%s" % (w[p11], w[p12])
            else:
                word1 += f"%s" % (w[p11])

        # Hunderter und Zehner/Einer größer als 0, dann muss ein 'and'davor
        if int(p2) > 0 and int(p1) > 0:
            word += f"and %s" % (word1)
        else:
            word = f"%s" % (word1)

    return word

# removes spaces and dashes
def cleanString(str):
    clean = str.replace(' ' , '')
    clean = clean.replace('-' , '')
    return clean

# determines len of all strings resulting bey the transformation
def solve(n):
    totalLen = 0
    for i in range(1, n + 1):
        word = toNumerWords(i, numberWords)
        totalLen += len(cleanString(word))
    return totalLen

# print all numbers until 10000
def testit():
    for i in range(1,1000+1):
        word = toNumerWords(i, numberWords)
        print(word)
        print(cleanString(word))

print(solve(1000))

# testit()

#
# nw = toNumerWords(342, numberWords)
# print(nw)
# print(len(cleanString(nw)))

# nw = toNumerWords(115, numberWords)
# print(nw)
# print(len(cleanString(nw)))

