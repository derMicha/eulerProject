#euler20
zahl = 1

# 100!
for i in range(1,100+1):
    zahl *= i


zahlStr = str(zahl)
sum = 0
for zifferStr in zahlStr:
    sum += int(zifferStr)

print(sum)
