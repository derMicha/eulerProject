import logging

FORMAT = '%(asctime)-20s [%(levelname)s]: %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)

def sumSquares(lower, upper):
    sum = 0
    for i in range(lower, upper + 1):
        sum += i ** 2
    return sum


def solve():
    lower = 1
    upper = 100
    s1 = sumSquares(lower, upper)
    s2 = sum(range(lower, upper + 1)) ** 2
    logging.info("%d - %d = %d" % (s2, s1, s2 - s1))


if __name__ == "__main__":
    FORMAT = '%(asctime)-20s [%(levelname)s]: %(message)s'
    logging.basicConfig(level=logging.INFO, format=FORMAT)
    solve()

