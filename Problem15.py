

def solve(kl):
    count=0
    for i in range(1, 2 ** (2 * kl),2):
        b = bin(i)
        bStr = str(b)
        if (bStr.count('1') == kl):
            # bStr = f"%0{kantenlänge}d" % (int(bStr[2:]))
            # lösungen.append(bStr)
            count+=1

    # print(lösungen)
    return count * 2

for i in range(2,13):
    print(f"Anzahl der Lösungen für Kantenlänge von %d : %d" % (i, solve(i)))

print("done")
